$(document).ready(function () {

    //vars
    var twitter_slider = $('#twitter_slider'),
        nav = $('#main_nav'),
        mob_btn = $('#mob_btn');
    
    //twitter slider
    twitter_slider.owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimout: 6000,
        autoplayHoverPause: true,
        autoplaySpeed: 800,
        loop: true
    });
    
    //mobile nav
    function toggle_nav() {
        if (nav.hasClass('open')) {
            //close
            nav.removeClass('open');
        } else {
            //open
            nav.addClass('open');
        }
    }
    
    mob_btn.click(toggle_nav);
    
});