$(document).ready(function () {

    //vars
    var twitter_slider = $('#twitter_slider'),
        nav = $('#main_nav'),
        mob_btn = $('#mob_btn');
    
    //twitter slider
    twitter_slider.owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimout: 6000,
        autoplayHoverPause: true,
        autoplaySpeed: 800,
        loop: true
    });
    
    //mobile nav
    function toggle_nav() {
        if (nav.hasClass('open')) {
            //close
            nav.removeClass('open');
        } else {
            //open
            nav.addClass('open');
        }
    }
    
    mob_btn.click(toggle_nav);
    
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vdmFyc1xuICAgIHZhciB0d2l0dGVyX3NsaWRlciA9ICQoJyN0d2l0dGVyX3NsaWRlcicpLFxuICAgICAgICBuYXYgPSAkKCcjbWFpbl9uYXYnKSxcbiAgICAgICAgbW9iX2J0biA9ICQoJyNtb2JfYnRuJyk7XG4gICAgXG4gICAgLy90d2l0dGVyIHNsaWRlclxuICAgIHR3aXR0ZXJfc2xpZGVyLm93bENhcm91c2VsKHtcbiAgICAgICAgaXRlbXM6IDEsXG4gICAgICAgIG5hdjogZmFsc2UsXG4gICAgICAgIGRvdHM6IHRydWUsXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgICAgICBhdXRvcGxheVRpbW91dDogNjAwMCxcbiAgICAgICAgYXV0b3BsYXlIb3ZlclBhdXNlOiB0cnVlLFxuICAgICAgICBhdXRvcGxheVNwZWVkOiA4MDAsXG4gICAgICAgIGxvb3A6IHRydWVcbiAgICB9KTtcbiAgICBcbiAgICAvL21vYmlsZSBuYXZcbiAgICBmdW5jdGlvbiB0b2dnbGVfbmF2KCkge1xuICAgICAgICBpZiAobmF2Lmhhc0NsYXNzKCdvcGVuJykpIHtcbiAgICAgICAgICAgIC8vY2xvc2VcbiAgICAgICAgICAgIG5hdi5yZW1vdmVDbGFzcygnb3BlbicpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy9vcGVuXG4gICAgICAgICAgICBuYXYuYWRkQ2xhc3MoJ29wZW4nKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICBtb2JfYnRuLmNsaWNrKHRvZ2dsZV9uYXYpO1xuICAgIFxufSk7Il19
